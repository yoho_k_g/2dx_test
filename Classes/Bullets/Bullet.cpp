﻿#pragma execution_character_set("utf-8")
#include "Bullet.h"
#include "BulletEvents\BulletFactory.h"
#include <cmath>

USING_NS_CC;


bool Bullet::init() {
	if (!Sprite::init()) {
		return false;
	}

	Bullet::scheduleUpdate();
	return true;
}

//次フレーム移動量計算
void Bullet::updateMove() {
	this->movePos.set(cosf(this->Angle), sinf(this->Angle));
	this->movePos *= this->Speed;
}

//とりあえず軸平行長方形だと思って実装
void Bullet::Reflection(Vec2 &pos, int dir) {
	Vec2 sizeWindow = Director::getInstance()->getWinSize();
	float sWidth = this->getContentSize().width;
	float sHeight = this->getContentSize().height;
	//右
	if (dir&DIR_RIGHT&(~RefMask)) {
		pos.x = 2.0f*sizeWindow.x - pos.x - sWidth;
		this->Angle = M_PI - this->Angle;
		RefCount--;
		updateMove();
	}
	//上
	if (dir&DIR_TOP&(~RefMask)) {
		pos.y = 2.0f*sizeWindow.y - pos.y - sHeight;
		this->Angle = 2.0f*M_PI - this->Angle;
		RefCount--;
		updateMove();
	}
	//左
	if (dir&DIR_LEFT&(~RefMask)) {
		pos.x = sWidth - pos.x;
		this->Angle = M_PI - this->Angle;
		RefCount--;
		updateMove();
	}
	//下
	if (dir&DIR_BOTTOM&(~RefMask)) {
		pos.y = sHeight - pos.y;
		this->Angle = 2.0f*M_PI - this->Angle;
		RefCount--;
		updateMove();
	}

}

void Bullet::OutofDisplay(int dir) {
	Die();
}

void Bullet::update(float delta) {
	//MovePosを計算
	updateMove();
	Vec2 pos = this->getPosition() + this->movePos;

	Vec2 sizeWindow = Director::getInstance()->getWinSize();
	float sWidth = this->getContentSize().width;
	float sHeight = this->getContentSize().height;

	int dir;
	if (RefCount >= 1) {
		//反射判定
		if (dir = Bounding(0.0f, sizeWindow.x, 0.0f, sizeWindow.y, pos)) {
			Reflection(pos,dir);
		}
	}
	//消滅判定
	dir = 0;
	if (dir = Bounding(-sWidth, sizeWindow.x + sWidth, -sHeight, sizeWindow.y + sHeight, pos)) {
		OutofDisplay(dir);
	}


	//移動確定
	this->setPosition(pos);
}


//サイズを持つ弾の画面境界[xmin~xmax,ymin~ymax]との衝突判定
int Bullet::Bounding(float xmin, float xmax, float ymin, float ymax, Vec2 pos) {
	float sWidth = this->getContentSize().width;
	float sHeight = this->getContentSize().height;
	int dir = 0;
	//右
	if (pos.x + sWidth / 2 >= xmax) {
		dir |= DIR_RIGHT;
	}
	//左
	else if (pos.x - sWidth / 2 <= xmin) {
		dir |= DIR_LEFT;
	}
	//上
	if (pos.y + sHeight / 2 >= ymax) {
		dir |= DIR_TOP;
	}
	//下
	else if (pos.y - sHeight / 2 <= ymin) {
		dir |= DIR_BOTTOM;
	}

	return dir;
}


void Bullet::Die() {
	this->setVisible(false);
	BulletFactory::getInstance()->DeadBulletList.insert(std::make_pair(this->classID, this));
	Bullet::unscheduleUpdate();
}
