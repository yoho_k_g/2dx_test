﻿#pragma execution_character_set("utf-8")
#include "AccelBullet.h"

USING_NS_CC;


void AccelBullet::updateMove() {
	this->movePos.set(cosf(this->Angle), sinf(this->Angle));
	this->movePos *= this->Speed;
	this->Speed = std::min(this->Speed + this->Accel,this->MaxSpeed);
	this->Angle += this->Alpha;
}
