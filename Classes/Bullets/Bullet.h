﻿#pragma execution_character_set("utf-8")
#pragma once
#include "cocos2d.h"
#include "BulletIDNames.h"

#define DIR_RIGHT 1<<0
#define DIR_TOP 1<<1
#define DIR_LEFT 1<<2
#define DIR_BOTTOM 1<<3

class Bullet;

//"パラメータ"(弾IDに依存しないSpeed,Angleなど)を受けて弾生成を行うクラス
struct AbstractBulletGen {
	static int const classID = 0;
	virtual Bullet* operator()(float Speed, float Angle) const = 0;
	virtual void SetBulletParam(Bullet* ptr, float Speed, float Angle) const = 0;
};


//弾の抽象クラス。SpeedとAngleで飛行する。画面反射も可能。
class Bullet : public cocos2d::Sprite
{
public:
	int const classID = CID_BULLET;	//弾クラスID
	int ID;	//弾ID
	int Side;	//弾の所属 0：自分 1：敵

	int RefCount = 0; //反射回数
	int RefMask = 0b1000;	//反射マスク(下左上右壁で反射しない)
	float Speed; //速度	(pixel/f)
	float Angle; //角	(rad)
	cocos2d::Vec2 BBox;
	virtual bool init();
	CREATE_FUNC(Bullet)

	//弾生成
	struct BulletGen : AbstractBulletGen {
	public:
		static int const classID = CID_BULLET;
		explicit BulletGen(int RefCount = 0, int RefMask = 0b1000) {
			this->RefCount = RefCount;
			this->RefMask = RefMask;
		}
		virtual Bullet* operator()(float Speed, float Angle) const{
			Bullet* ptr;
			ptr = Bullet::create();
			SetBulletParam(ptr, Speed, Angle);
			return ptr;
		}
		virtual void SetBulletParam(Bullet* ptr, float Speed, float Angle) const{
			ptr->RefCount = this->RefCount;
			ptr->RefMask = this->RefMask;
			ptr->Speed = Speed;
			ptr->Angle = Angle;
		}
	protected:
		int RefCount;
		int RefMask;

	};

protected:
	cocos2d::Vec2 movePos;	//次フレーム移動距離
	virtual void updateMove();	//次フレーム移動距離の設定
	virtual void Reflection(cocos2d::Vec2 &pos,int dir) ;	//壁衝突
	virtual void OutofDisplay(int dir); //画面外
	virtual void update(float delta);
	virtual int Bounding(float xmin, float xmax, float ymin, float ymax, cocos2d::Vec2 pos); //画面境界との衝突判定
	virtual void Die();	//弾の死亡
};

