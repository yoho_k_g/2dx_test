﻿#pragma execution_character_set("utf-8")
#pragma once
#include "BulletIDNames.h"
#include "Bullet.h"

//加速度と最高速度、角加速を持つ弾クラス
class AccelBullet :public Bullet
{
public:
	int const classID = CID_ACCEL_BULLET;	//弾クラスID
	CREATE_FUNC(AccelBullet)

	struct AccelBulletGen : BulletGen {
		static int const classID = CID_ACCEL_BULLET;
		float Accel;
		float Alpha;
		float MaxSpeed = 1024.0f;
		//加速度・最高速・(角速度)・反射カウント・反射マスク
		explicit AccelBulletGen(float Accel, float MaxSpeed = 1024.0f, float Alpha = 0.0f, int RefCount = 0, int RefMask = 0b1000) : BulletGen(RefCount, RefMask) {
			this->Accel = Accel;
			this->Alpha = Alpha;	
			this->MaxSpeed = MaxSpeed;
		}
		AccelBullet* operator()(float Speed, float Angle) const{
			AccelBullet* ptr;
			ptr = AccelBullet::create();
			SetBulletParam(ptr, Speed, Angle);
			return ptr;
		}
		virtual void SetBulletParam(AccelBullet* ptr, float Speed, float Angle) const{
			ptr->RefCount = this->RefCount;
			ptr->RefMask = this->RefMask;
			ptr->Speed = Speed;
			ptr->Angle = Angle;
			ptr->Accel = this->Accel;
			ptr->Alpha = this->Alpha;
			ptr->MaxSpeed = this->MaxSpeed;
		}
	};
protected:
	float Accel;	//加速度(pixel/f^2)
	float Alpha;	//角速度(rad/f)
	float MaxSpeed = 0.0f; //限界速度 (pixel/f)
	virtual void AccelBullet::updateMove();
};

