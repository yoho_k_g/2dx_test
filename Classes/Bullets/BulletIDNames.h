﻿#pragma execution_character_set("utf-8")
#pragma once


//BulletClassID

const int CID_BULLET = 0;
const int CID_ACCEL_BULLET = 1;


/****************
BulletID Format
s_***
s  :Side (Player:0 Enemy: 1)
***************/


//PlayerBullet 000~999
const int BID_SMALL = 91;
const int BID_LARGE = 92;

//EnemyBullet 1000~1999

