﻿#pragma execution_character_set("utf-8")

#define _USE_MATH_DEFINES
#include "Player.h"
#include "WeaponManager.h"
#include <cmath>

USING_NS_CC;

Size WinSize;
std::map<cocos2d::EventKeyboard::KeyCode, int> keymap;
Vec2 mousepos(0.0f,0.0f);


bool Player::init() {
	if (!Sprite::initWithFile("CloseNormal.png")) {
		return false;
	}
	//画面サイズ
	this->WinSize = Director::getInstance()->getWinSize();

	//上向き
	mousepos.set(this->WinSize.width / 2, this->WinSize.height / 2);

	//キー操作登録
	auto listener_key = EventListenerKeyboard::create();
	listener_key->onKeyPressed = CC_CALLBACK_2(Player::keyPress, this);
	listener_key->onKeyReleased = CC_CALLBACK_2(Player::keyRelease, this);
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener_key, this);

	//マウス操作登録
	auto listener_mouse = EventListenerMouse::create();
	listener_mouse->onMouseMove = CC_CALLBACK_1(Player::mouseMove,this);
	listener_mouse->onMouseDown = CC_CALLBACK_1(Player::mousePress, this);
	listener_mouse->onMouseUp = CC_CALLBACK_1(Player::mouseRelease, this);
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener_mouse, this);

	//武器設定
	wm = new WeaponManager();
	this->addChild(wm->getWeapon0());
	this->addChild(wm->getWeapon1());

	Player::scheduleUpdate();

	return true;
}

//キー押下
void Player::keyPress(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
	this->keymap[keyCode] = 1;
}

//キー押上
void Player::keyRelease(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
	this->keymap[keyCode] = 0;
}

//マウス位置更新
void Player::mouseMove(cocos2d::Event* event) {
	EventMouse* e = (EventMouse*)event;
	this->mousepos.set(e->getCursorX(),e->getCursorY());
}

//マウスクリック
void Player::mousePress(cocos2d::Event* event) {
	EventMouse* e = (EventMouse*)event;
	//左クリックON
	if (e->getMouseButton() == 0) {
		ismouseLeft = true;
	}
	if (e->getMouseButton() == 1) {
		wm->ToggleWeapon();
	}
}
void Player::mouseRelease(cocos2d::Event* event) {
	EventMouse* e = (EventMouse*)event;
	//左クリックOFF
	if (e->getMouseButton() == 0) {
		ismouseLeft = false;
	}

}

void Player::update(float delta)
{

	//キー移動
	int X = this->keymap[EventKeyboard::KeyCode::KEY_D] - this->keymap[EventKeyboard::KeyCode::KEY_A];
	int Y = this->keymap[EventKeyboard::KeyCode::KEY_W] - this->keymap[EventKeyboard::KeyCode::KEY_S];
	Vec2 moveVec(X*4.5f,Y*4.5f);
	Vec2 pos = this->getPosition();
	pos += moveVec;
	pos.clamp(Vec2(0.0f,0.0f),Vec2(this->WinSize.width,this->WinSize.height));
	this->setPosition(pos);

	//マウス追従回転
	float Angle = CC_RADIANS_TO_DEGREES(M_PI/2 - (this->mousepos - pos).getAngle());
	this->setRotation(Angle);

	//武器発射
	if (ismouseLeft) {
		wm->getWeapon0()->Trigger = true;
	}
	else {
		wm->getWeapon0()->Trigger = false;
	}

}


Player::~Player() {
	delete(wm);
}
