﻿#pragma once
#pragma execution_character_set("utf-8")
#include "cocos2d.h"
#include "Weapons\WeaponIDNames.h"
#include "Bullets\BulletIDNames.h"
#include "Weapons\Weapon.h"
#include "Weapons\ProjectileWeapon.h"
#include <map>

//プレイヤーの武器管理
class WeaponManager
{
private:
	Weapon* WeaponPair[2];
public:
	WeaponManager();
	~WeaponManager();
	void ToggleWeapon();
	Weapon* getWeapon0();
	Weapon* getWeapon1();
};

