﻿#pragma once
#pragma execution_character_set("utf-8")
#include "cocos2d.h"
#include "WeaponManager.h"


//プレイヤー本体のクラス。入力のハンドルを行う。
class Player : public cocos2d::Sprite
{
public:
	int HP;
	CREATE_FUNC(Player)
private:
	cocos2d::Size WinSize;
	std::map<cocos2d::EventKeyboard::KeyCode, int> keymap;
	cocos2d::Vec2 mousepos;
	bool ismouseLeft;
	WeaponManager* wm;

	virtual bool init();
	void keyPress(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void keyRelease(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void mouseMove(cocos2d::Event* event);
	void mousePress(cocos2d::Event* event);
	void mouseRelease(cocos2d::Event* event);
	void update(float delta);
	~Player();
};

