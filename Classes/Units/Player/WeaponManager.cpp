﻿#pragma execution_character_set("utf-8")
#include "WeaponManager.h"

USING_NS_CC;


WeaponManager::WeaponManager()
{

	//ファイルロードで武器の設定を読む(予定)
	WeaponPair[0] = ProjectileWeapon::create();
	WeaponPair[1] = ProjectileWeapon::create();

	WeaponPair[0]->weapon_ID = WID_PROJECTILE_SMALL;
	WeaponPair[1]->weapon_ID = WID_PROJECTILE_LARGE;

	for (int i = 0; i <= 1; i++) {
		Weapon* wp = WeaponPair[i];

		//武器タイプごと
		switch (wp->weapon_ID / 1000) {
		case WID_PROJECTILE:
			ProjectileWeapon* pwp = (ProjectileWeapon*)wp;
			//initParamWithID(wp->weapon_ID)
			//下は現時点でLoadFileの代わり
			pwp->CoolDown = 3;
			if (i == 0) {
				pwp->bullet_ID = 91;
			}
			else {
				pwp->bullet_ID = 92;
			}
			pwp->Power = 5.0f;
			pwp->nWay = 3;
			pwp->Speed = 10.0f;
			pwp->SpreadAngle = 20.0f;
			pwp->Accel = 0.0f;
			pwp->Alpha = 0.0f;
			pwp->RefCount = 0;
			pwp->RefMask = 0;
			break;
		}

		//武器ごとに画像ロード
		switch (wp->weapon_ID) {
		case WID_PROJECTILE_SMALL:
			wp->initWithFile("Weapons/1000_projectile_small.png");
			break;
		case WID_PROJECTILE_LARGE:
			wp->initWithFile("Weapons/1001_projectile_large.png");
			break;
		}

		wp->setAnchorPoint(Vec2(0.5f,0.0f));
		wp->setPosition(15.0f,20.0f);

	}

	//第二武器テスト用コード
	ProjectileWeapon* pwp = (ProjectileWeapon*)WeaponPair[1];
	pwp->CoolDown = 10;
	pwp->bullet_ID = 92;
	pwp->nWay = 1;
	pwp->Speed = 4.0f;
	pwp->Accel = 0.3f;
	pwp->Alpha = 0.0f;
	pwp->RefCount = 1;
	pwp->RefMask = 0b1010;

	WeaponPair[1]->setVisible(false);
}

void WeaponManager::ToggleWeapon() {
	WeaponPair[1]->Trigger = WeaponPair[1]->Trigger;
	WeaponPair[0]->Trigger = false;
	WeaponPair[0]->setVisible(false);

	Weapon* tmp = WeaponPair[1];
	WeaponPair[1] = WeaponPair[0];
	WeaponPair[0] = tmp;

	WeaponPair[0]->setVisible(true);
}

Weapon* WeaponManager::getWeapon0() {
	return WeaponPair[0];
}
Weapon* WeaponManager::getWeapon1() {
	return WeaponPair[1];
}

WeaponManager::~WeaponManager()
{
}


