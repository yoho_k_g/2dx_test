﻿#pragma execution_character_set("utf-8")
#include "Weapon.h"

USING_NS_CC;

bool Weapon::init() {
	if (!Sprite::init()) {
		return false;
	}
	Weapon::scheduleUpdate();
	return true;
}

void Weapon::update(float delta) {
	this->Reload--;
	if (this->Reload <= 0 && this->Trigger) {
		this->Reload = this->CoolDown;
		this->shoot();
	}
}