﻿#pragma execution_character_set("utf-8")
#include "ProjectileWeapon.h"
#include "BulletEvents\BulletEvent.h"
#include "Bullets\AccelBullet.h"

USING_NS_CC;

void ProjectileWeapon::shoot() {
	BulletEvent::nWayProjection(this->getParent()->convertToWorldSpace(this->getPosition()+Vec2(0.0,20.0f)), this->bullet_ID, this->nWay, this->Speed, 90 - 1 * this->getParent()->getRotation(), this->SpreadAngle,Bullet::BulletGen(this->RefCount,this->RefMask));
}