﻿#pragma once
#pragma execution_character_set("utf-8")
#include "cocos2d.h"
#include "Weapon.h"

//初期状態を持つ弾を発射するWeaponのクラス
class ProjectileWeapon : public Weapon
{
public:
	int bullet_ID;	//弾ID
	float Power;	//攻撃力
	int nWay=1;		//Way数
	float Speed;	//弾速
	float SpreadAngle=0.0f;	//拡散(deg)
	float Accel=0.0f;	//加速度
	float Alpha=0.0f;	//角加速度
	int RefCount = 0;
	int RefMask = 0b1000;
	CREATE_FUNC(ProjectileWeapon)
//	virtual bool init();
protected:
	virtual void shoot();
};

