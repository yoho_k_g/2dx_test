﻿#pragma once
#pragma execution_character_set("utf-8")
#include "cocos2d.h"

class Weapon : public cocos2d::Sprite
{
public:
	int weapon_ID;
	int CoolDown=20;
	bool Trigger;
	virtual bool init();
protected:
	int Reload;
	virtual void update(float delta);
	virtual void shoot()=0;
};

