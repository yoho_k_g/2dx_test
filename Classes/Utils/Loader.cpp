﻿#pragma execution_character_set("utf-8")
#include <string>

#include "Loader.h"



std::unique_ptr<BulletInfo>  Loader::BulletInfoArray[2000];

void Loader::init() {
	for (int i = 0; i < 2000; i++) {
		BulletInfoArray[i] = make_unique<BulletInfo>();
	}

}

void Loader::LoadBulletInfo() {
	vector<rapidjson::Document> DocumentVector;
	const string path = "Scripts/BulletInfo.json";
	setDataFromJson(path,[](rapidjson::Document& doc) {
		const rapidjson::Value& BulletInfoDoc = doc["BulletInfoList"];
		for (rapidjson::SizeType i = 0; i < BulletInfoDoc.Size(); i++) {
			int Id = BulletInfoDoc[i]["Id"].GetInt();
			BulletInfoArray[Id]->Id = Id;
			BulletInfoArray[Id]->Name = BulletInfoDoc[i]["Name"].GetString();
			BulletInfoArray[Id]->Image = BulletInfoDoc[i]["Image"].GetString();
			//増えたらまともにする
			string BStr = BulletInfoDoc[i]["BType"].GetString();
			int BType=0;
			if (BStr == "Box") {
				BType = 0;
			}
			BulletInfoArray[Id]->BType = BType;
			BulletInfoArray[Id]->Size = BulletInfoDoc[i]["Size"].GetFloat();
			BulletInfoArray[Id]->RefCount = BulletInfoDoc[i]["RefCount"].GetInt();
			BulletInfoArray[Id]->RefMask = BulletInfoDoc[i]["RefMask"].GetInt();
			BulletInfoArray[Id]->Mover = BulletInfoDoc[i]["Mover"].GetString();
		}
	}
	);
	
}

const BulletInfo& Loader::getBulletInfo(int Bullet_ID) {
	return *BulletInfoArray[Bullet_ID];
}

void Loader::setDataFromJson(const std::string &path, std::function<void(rapidjson::Document&)> DataWriter) {
	auto fileUtils = cocos2d::FileUtils::getInstance();

	rapidjson::Document doc;
	doc.Parse(fileUtils->getStringFromFile(path).c_str());
	if (doc.HasParseError()) {
		_RPTFN(0, "ParseError:%s\n", path);
	}
	else {
		DataWriter(doc);
	}


}