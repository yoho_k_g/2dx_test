﻿#pragma once
#pragma execution_character_set("utf-8")

#include <String>
#include <vector>
#include <memory>
#include "json\rapidjson.h"
#include "json\document.h"
#include "cocos2d.h"

using namespace std;

//
struct BulletInfo {
public:
	int Id;
	string Name;
	string Image;
	int BType;
	float Size;	//現在パラメータは1つ
	int RefCount;
	int RefMask;
	string Mover;
};


//スクリプトデータのメモリ管理をするクラス
class Loader
{
public:
//API
	static void init();
	//BulletInfo(json)の読み込み
	static void LoadBulletInfo();
	//メモリ上のBulletInfoを取得
	static const BulletInfo& getBulletInfo(int Bullet_ID);

private:
	static std::unique_ptr<BulletInfo> BulletInfoArray[2000];

//Library
	//ディレクトリ内jsonファイルのDocumentを全取得
	static void setDataFromJson(const std::string &path, std::function<void(rapidjson::Document&)> DataWriter);

	Loader() {};
	~Loader() {};
};

