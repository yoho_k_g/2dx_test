﻿#pragma execution_character_set("utf-8")
#pragma once
#include "cocos2d.h"
#include "Bullets\Bullet.h"

//弾生成関数群
class BulletEvent
{
private:
	BulletEvent() {};
	~BulletEvent() {};
public:


//BulletEvents
	//弾発射。原点・弾ID・狙い角(deg)
	static void BulletEvent::Projection(cocos2d::Vec2 Origin, int bullet_ID, float Speed, float TargetAngle, const AbstractBulletGen& BulletGenerator);
	//nWay弾発射。原点・弾ID・個数・スピード・狙い角(deg)・弾間隔(deg)
	static void BulletEvent::nWayProjection(cocos2d::Vec2 Origin, int bullet_ID, int num, float Speed, float TargetAngle, float SpreadAngle, const AbstractBulletGen& BulletGenerator);
	//円形弾発射。原点・弾ID・個数・スピード・狙い角(deg)
	static void BulletEvent::CircleProjection(cocos2d::Vec2 Origin, int bullet_ID, int num, float Speed, float TargetAngle, const AbstractBulletGen& BulletGenerator);

};

