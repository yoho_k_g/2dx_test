﻿#pragma execution_character_set("utf-8")

#include <memory>
#include "json/rapidjson.h"
#include "BulletFactory.h"
#include "Bullets\BulletIDNames.h"
#include "Utils\Loader.h"


USING_NS_CC;


Bullet* BulletFactory::MakeBullet(int Bullet_ID, float Speed, float Angle,const AbstractBulletGen &BulletGenerator) {
	Bullet* ptr;

	//リサイクル
	auto itr = this->DeadBulletList.find(BulletGenerator.classID);
	if (itr != this->DeadBulletList.end()) {
		ptr = itr->second;
		this->DeadBulletList.erase(itr);
		setInfo(Bullet_ID, ptr);
		BulletGenerator.SetBulletParam(ptr,Speed,Angle);
		ptr->setVisible(true);
		ptr->scheduleUpdate();
		return ptr;
	}

	//新規作成
	ptr = BulletGenerator(Speed,Angle);
	setInfo(Bullet_ID, ptr);
	Director::getInstance()->getRunningScene()->getChildByTag(0)->addChild(ptr);
	return ptr;
}

void BulletFactory::setInfo(int Bullet_ID, Bullet* pBullet) {
	const BulletInfo& BInfo = Loader::getBulletInfo(Bullet_ID);
	pBullet->ID = BInfo.Id;
	pBullet->initWithFile(BInfo.Image);
}