﻿#pragma execution_character_set("utf-8")
#pragma once
#include "Classes\Scenes\Stage\PlayLayer.h"
#include "Bullets\Bullet.h"
#include <map>

//指定したIDの弾のポインタを生成(orリサイクル)して渡すシングルトン。外部情報(BulletGenの情報と弾ID非依存情報(from スクリプト)、Speed、Angleの設定もする。
class BulletFactory
{
public:
	static BulletFactory* getInstance() {
		static BulletFactory bf;
		return &bf;
	};
	std::unordered_multimap<int, Bullet*> DeadBulletList;	//画面外に消えた弾の連想配列 <type_ID,Bulletポインタ>
	//弾を生成orリサイクルしてポインタを返す
	Bullet* MakeBullet(int Bullet_ID, float Speed, float Angle, const AbstractBulletGen &ABG);

private:
	void setInfo(int Bullet_ID, Bullet *pBullet);
	BulletFactory() {};
	~BulletFactory() {};
};


