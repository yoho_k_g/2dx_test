﻿#pragma execution_character_set("utf-8")
#include "BulletEvent.h"
#include "BulletFactory.h"


USING_NS_CC;



void BulletEvent::Projection(cocos2d::Vec2 Origin, int Bullet_ID, float Speed, float TargetAngle, const AbstractBulletGen& BulletGenerator) {
	nWayProjection(Origin,Bullet_ID,1,Speed,TargetAngle,0.0f,BulletGenerator);
}
void BulletEvent::nWayProjection(cocos2d::Vec2 Origin, int Bullet_ID, int num, float Speed, float TargetAngle, float SpreadAngle, const AbstractBulletGen& BulletGenerator) {
	float angle = TargetAngle - SpreadAngle*((float)(num - 1) / 2.0f);
	for (int i = 0; i < num; i++) {
		Bullet* pBullet = BulletFactory::getInstance()->MakeBullet(Bullet_ID,Speed,angle,BulletGenerator);
		pBullet->setPosition(Origin);
		pBullet->Angle = CC_DEGREES_TO_RADIANS(angle); angle += SpreadAngle;
	}
}
void BulletEvent::CircleProjection(cocos2d::Vec2 Origin, int Bullet_ID, int num, float Speed,float TargetAngle,const AbstractBulletGen& BulletGenerator) {
	float angle = TargetAngle;
	float dangle = 360.0f / num;
	for (int i = 0; i < num; i++) {
		Bullet* pBullet = BulletFactory::getInstance()->MakeBullet(Bullet_ID,Speed, angle, BulletGenerator);
		angle += dangle;
	}
}
