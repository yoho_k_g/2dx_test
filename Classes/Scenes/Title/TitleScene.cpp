﻿#pragma execution_character_set("utf-8")
#include "SimpleAudioEngine.h"
#include "Utils\Loader.h"
#include "TitleScene.h"
#include "TitleLayer.h"
#include "Scenes\Stage\StageScene.h"

USING_NS_CC;

Scene* TitleScene::createScene() {
	auto scene = TitleScene::create();

	//Layer1:TitleLayer
	auto layer = TitleLayer::create();
	scene->addChild(layer);
	return scene;
}

bool TitleScene::init() {
	if (!Scene::init()) {
		return false;
	}
	//ロード
	Loader::init();
	Loader::LoadBulletInfo();


	//キー入力でシーン切り替えイベント
	auto listener_key = EventListenerKeyboard::create();
	listener_key->onKeyPressed = CC_CALLBACK_2(TitleScene::KeyPressed, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener_key, this);
	return true;
}

//キー入力で画面遷移
void TitleScene::KeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) {
	
	Director::getInstance()->replaceScene(TransitionFade::create(2.0f, StageScene::createScene(),Color3B::BLACK));
//	_RPTFN(_CRT_WARN,"Key with keycode:%d\n",keyCode);
}
