﻿#pragma execution_character_set("utf-8")
#pragma once

#include "cocos2d.h"

class TitleScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void KeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

	CREATE_FUNC(TitleScene);
};

