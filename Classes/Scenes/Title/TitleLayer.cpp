﻿#pragma execution_character_set("utf-8")
#include "TitleLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

bool TitleLayer::init()
{
	if (!Layer::init())
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//BGColor
	auto bg = LayerColor::create(Color4B(255,255,255,255),visibleSize.width,visibleSize.height);
	this->addChild(bg);

	//Label：PUSH ANY KEY
	auto label_start = Label::createWithTTF("PUSH ANY KEY", "fonts/meiryo.ttc", 28);
	label_start->setPosition(Vec2(origin.x + visibleSize.width/2,origin.y + visibleSize.height/4));
	label_start->setColor(Color3B(0,0,0));
	this->addChild(label_start,1);

	return true;
}