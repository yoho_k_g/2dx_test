﻿#pragma execution_character_set("utf-8")
#include "SimpleAudioEngine.h"
#include "PlayLayer.h"
#include "Units\Player\Player.h"

USING_NS_CC;

bool PlayLayer::init()
{
	if (!Layer::init())
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//BGColor
	auto bg = LayerColor::create(Color4B(255, 255, 255, 255), visibleSize.width, visibleSize.height);
	this->addChild(bg);

	//プレイヤーの生成
	this->initPlayer();

	this->scheduleUpdate();
	return true;
}

void PlayLayer::initPlayer() {
	Size winSize = Director::getInstance()->getWinSize();
	auto player = Player::create();
	player->HP = 3;
	player->setPosition(winSize.width*0.5,winSize.height*0.2);
	this->addChild(player);
}


void PlayLayer::update(float delta) {
}

