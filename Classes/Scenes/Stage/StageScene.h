﻿#pragma execution_character_set("utf-8")
#pragma once
#include "cocos2d.h"


class StageScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	CREATE_FUNC(StageScene);
};

