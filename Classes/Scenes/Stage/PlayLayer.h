﻿#pragma execution_character_set("utf-8")
#pragma once
#include "cocos2d.h"

//実際にプレイヤーや敵が動くレイヤー
class PlayLayer : public cocos2d::Layer
{
public:
	CREATE_FUNC(PlayLayer)
	virtual bool init();
	void initPlayer();
	void update(float delta);
};

