﻿#pragma execution_character_set("utf-8")
#include "StageScene.h"
#include "PlayLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

//ステージをプレイしているシーン
Scene* StageScene::createScene()
{
	auto scene = StageScene::create();

	//1:PlayLayer
	auto layer1 = PlayLayer::create();
	scene->addChild(layer1,5,0);

	return scene;
}

bool StageScene::init() {

	return true;
}